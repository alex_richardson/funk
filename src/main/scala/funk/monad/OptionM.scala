package funk.monad

sealed trait OptionM[+T] {
  def map[B](f: T => B): OptionM[B]
  def flatMap[B](f: T => OptionM[B]): OptionM[B]
  def filter(f: T => Boolean): OptionM[T]
  def ++[A >: T](other: OptionM[A]): OptionM[A]
}
object OptionM{
  def apply[B](b: Seq[B]): OptionM[B] = b.toList match {
    case Nil => Zero
    case x :: Nil => One(x)
    case x :: xs => Many(x :: xs)
  }
}
case object Zero extends OptionM[Nothing] { self =>
  override def map[B](f: Nothing => B): OptionM[B] = self

  override def flatMap[B](f: Nothing => OptionM[B]): OptionM[B] = self

  override def ++[Nothing](other: OptionM[Nothing]): OptionM[Nothing] = other

  override def filter(f: (Nothing) => Boolean): OptionM[Nothing] = self
}

case class One[T](x: T) extends OptionM[T] { self =>
  override def map[B](f: (T) => B): OptionM[B] = One(f(x))

  override def flatMap[B](f: (T) => OptionM[B]): OptionM[B] = f(x)

  override def ++[A >: T](other: OptionM[A]): OptionM[A] = other match {
    case Zero => self
    case One(item) => Many(Seq(x, item))
    case Many(items) => Many(x +: items)
  }

  override def filter(f: T => Boolean): OptionM[T] = if(f(x)) self else Zero
}

case class Many[T](xs: Seq[T]) extends OptionM[T] { self =>
  override def map[B](f: (T) => B): OptionM[B] = Many(xs.map(f))

  override def flatMap[B](f: (T) => OptionM[B]): OptionM[B] = xs.map(f).foldLeft[OptionM[B]](Zero)(_ ++ _)

  override def ++[A >: T](other: OptionM[A]): OptionM[A] = other match {
    case Zero => self
    case One(item) => Many(xs :+ item)
    case Many(items) => Many(xs ++ items)
  }

  override def filter(f: (T) => Boolean): OptionM[T] = OptionM(xs.filter(f))
}

case object MaybeOne {
  def unapply[T](a: OptionM[T]): Option[Option[T]] = a match {
    case Zero => Some(None)
    case One(elem) => Some(Some(elem))
    case Many(_) => None //MaybeOne is only for a single element
  }
}
