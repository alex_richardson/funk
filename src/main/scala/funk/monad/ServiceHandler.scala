package funk.monad

import scala.concurrent.{ExecutionContext, Future}
import scalaz._

sealed trait ServiceHandler[T] {
  self =>
  final def map[B](f: T => B) = flatMap(a => Raw(Future.successful(Some(f(a)))))

  final def flatMap[B](f: T => ServiceHandler[B]): ServiceHandler[B] = FlatMap(self, f)

  final def run(implicit executionContext: ExecutionContext): Future[Option[T]] =
    self match {
      case Raw(value) => value
      case FlatMap(current, f) => current match {
        case Raw(v) => f(v).run
        case FlatMap(y, g) => y.flatMap(z => g(z).flatMap(f)).run
      }
    }
}
//TODO Could probably model with a free monad internally
final case class Raw[T](f: Future[Option[T]]) extends ServiceHandler[T]
final case class FlatMap[A, B](current: ServiceHandler[A], f: A => ServiceHandler[B]) extends ServiceHandler[B]

object ServiceHandler extends Monad[ServiceHandler] {

  def apply[T](core: Future[Option[T]]) = Raw(core)

  override def bind[A, B](fa: ServiceHandler[A])(f: (A) => ServiceHandler[B]): ServiceHandler[B] = fa.flatMap(f)

  override def point[A](a: => A): ServiceHandler[A] = Raw(Future.successful(Some(a)))
}