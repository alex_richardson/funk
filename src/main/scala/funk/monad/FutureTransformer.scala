package funk.monad

import scala.concurrent.{ExecutionContext, Future}
import scalaz.Monad

case class FutureTransformer(implicit executionContext: ExecutionContext) extends Monad[Future] {
  override def bind[A, B](fa: Future[A])(f: (A) => Future[B]): Future[B] = fa.flatMap(f)

  override def point[A](a: => A): Future[A] = Future(a)
}
