package funk.request

import funk.request.Request.Interpretable
import funk.service.Service

import scalaz._

sealed trait Request[T]
final case class Pure[T](value: T) extends Request[T]
final case class Fetch[T](service: Service[T]) extends Request[T]
final case class MultiFetch[T, M[X] <: Traversable[X]](requests: M[Interpretable[T]]) extends Request[M[T]]
final case class Recursive[T](interpretable: Interpretable[T]) extends Request[T]
final case class Fork[A, B](left: Request[A], right: Request[B]) extends Request[(A, B)]

object Request {

  import Free._

  type Interpretable[T] = Free[Request, T]

  implicit def interpretableMonoid[T](implicit m: Monoid[T]): Monoid[Interpretable[T]] = new Monoid[Interpretable[T]] {
    override def zero: Interpretable[T] = pure(m.zero)

    override def append(f1: Interpretable[T], f2: => Interpretable[T]): Interpretable[T] =
      fork(f1, f2) map {case (a, b) => m.append(a, b)}
  }

  implicit val interpretableApplicative: Applicative[Interpretable] = new Applicative[Interpretable] {
    override def point[A](a: => A): Interpretable[A] = Request.pure(a)

    override def ap[A, B](fa: => Interpretable[A])(f: => Interpretable[(A) => B]): Interpretable[B] = {
      fork(fa, f) map { case (a, func) => func(a) }
    }
  }

  implicit class RunnableInterpretable[T](interpretable: Interpretable[T]) {
    def runWith[B[_]](interpreter: Request ~> B)(implicit m: Monad[B]) = interpretable.foldMap(interpreter)

    def get[B[_]](implicit interpreter: Request ~> B, m: Monad[B]) = runWith(interpreter)(m)
  }

  implicit val sequenceMonad = new Monad[Seq] {
    override def bind[A, B](fa: Seq[A])(f: (A) => Seq[B]): Seq[B] = fa.flatMap(f)

    override def point[A](a: => A): Seq[A] = Seq(a)
  }

  def pure[T](value: T): Interpretable[T] = {
    liftF(Pure(value))
  }

  def fetch[T](service: Service[T]): Interpretable[T] = {
    liftF(Fetch(service))
  }

  def fork[A,B](serviceA: Service[A], serviceB: Service[B]): Interpretable[(A,B)] = {
    liftF(Fork(Fetch(serviceA), Fetch(serviceB)))
  }

  def fork[A, B](interpretableA: Interpretable[A], interpretableB: Interpretable[B]): Interpretable[(A, B)] = {
    liftF(Fork(Recursive(interpretableA), Recursive(interpretableB)))
  }

  def sequence[A, M[X] <: Traversable[X]](calls: M[Interpretable[A]]): Interpretable[M[A]] =
   liftF(MultiFetch(calls))

  def traverse[A, M[X] <: Traversable[X]](source: M[A])(f: A => Interpretable[A])(implicit monad: Monad[M]): Interpretable[M[A]] = {
    sequence(monad.map(source)(f))
  }

}