package funk.interpreter

import funk.request._
import funk.service.Service

import scalaz._

case class RequestInterpreter[M[_]](serviceInterpreter: ServiceInterpreter[M])(implicit monad: Monad[M]) extends (Request ~> M) {
  override def apply[A](fa: Request[A]): M[A] = fa match {
    case Pure(a) => monad.point(a)
    case Fetch(service) => serviceInterpreter(service)
    case Fork(left, right) => {
      val a = this(left)
      val b = this(right)
      monad.bind(a)(x => monad.bind(b)(y => monad.point(x, y)))
    }
    case MultiFetch(requests) => invert(requests.map(x => this(Recursive(x))))
    case Recursive(loop) => loop.foldMap(this)
  }

  //TODO inline this in a foldLeft
  def invert[A](stuff: Traversable[M[A]]): M[Traversable[A]] = stuff.toList match {
    case x :: xs => monad.bind(x)(head => monad.map(invert(xs))(rest => Traversable(head) ++ rest))
    case Nil => monad.point(Nil)
  }
}


trait ServiceInterpreter[T[_]] {
  def apply[A](service: Service[A]): T[A]
}
