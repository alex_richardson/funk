package funk.request

import funk.interpreter.{RequestInterpreter, ServiceInterpreter}
import funk.request.Request._
import funk.service.Service
import org.specs2.mutable.Specification
import org.specs2.specification

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scalaz.Scalaz._
import scalaz._

class RequestTest extends Specification {

  import Id._

  case class TestServiceValue[T](value: T) extends Service[T]
  object TestServiceInterp extends ServiceInterpreter[Id] {
    override def apply[A](service: Service[A]): Id[A] = service match {
      case TestServiceValue(value) => value
    }
  }

  trait Scope extends specification.Scope{
    val interpreter = RequestInterpreter(TestServiceInterp)
    val failingInterpreter = RequestInterpreter(new ServiceInterpreter[Id] {
      override def apply[A](service: Service[A]): Id[A] = throw new IllegalArgumentException("shouldn't be here")
    })
  }

  trait ConcurrentScope extends Scope {
    var flagA = false
    var flagB = false
    case class FutureTest(switch: Boolean) extends Service[Boolean]
    object FutureInterpreter extends ServiceInterpreter[Future] {
      override def apply[A](service: Service[A]): Future[A] = service match {
        case FutureTest(true) => {
          flagA = true
          Promise[Boolean]().future
        }
        case FutureTest(false) => {
          flagB = true
          Promise[Boolean]().future
        }
      }
    }
    val futureInterpreter = RequestInterpreter(FutureInterpreter)
  }

  "Fetch makes a simple fetch" in new Scope {
    fetch(TestServiceValue("test")).runWith(interpreter) must_== "test"
  }

  "Pure bypasses the interpreter" in new Scope {
    pure("pure-value").runWith(failingInterpreter) must_== "pure-value"
  }

  "Fork creates two concurrent requests" in new ConcurrentScope {
    fork(FutureTest(true), FutureTest(false)).foldMap(futureInterpreter)
    flagA must beTrue
    flagB must beTrue
  }

  "sequence -> Seq[Interpretable] becomes Interpretable[Seq]" in new ConcurrentScope {
    val toRun = Seq(fetch(FutureTest(true)), fetch(FutureTest(false)))
    sequence(toRun).runWith(futureInterpreter)
    flagA must beTrue
    flagB must beTrue
  }

  //TODO this is still a little nasty, could model it better.
  "traverse -> Seq[T] & f: T => Interpretable[T] becomes Interpretable[Seq]" in new ConcurrentScope {
    val toRun = Seq(true, false)
    traverse(toRun)(switch => fetch(FutureTest(switch))).runWith(futureInterpreter)
    flagA must beTrue
    flagB must beTrue
  }

  "Applicative runs in parallel" in new ConcurrentScope {
    val res: Interpretable[(Boolean, Boolean)] = {fetch(FutureTest(true)) |@| fetch(FutureTest(false))}.tupled
    res.runWith(futureInterpreter)
    flagA must beTrue
    flagB must beTrue
  }
}