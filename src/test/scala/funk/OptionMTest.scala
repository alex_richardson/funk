package funk

import funk.monad._
import org.specs2.execute.{Failure, FailureException}
import org.specs2.mutable.Specification


class OptionMTest extends Specification {

  "The OptionM apply method" should {
    "Construct a Zero" in {
      OptionM(Seq.empty) must_== Zero
    }

    "Construct a One" in {
      OptionM(Seq(1)) must_== One(1)
    }

    "Construct a Many" in {
      OptionM(Seq(1,2,3)) must_== Many(Seq(1,2,3))
    }
  }

  "MaybeOne should" should {
    "match a one as a Some(obj)" in {
      One(1) match {
        case MaybeOne(opt) => opt must_== Some(1)
        case _ => throw new FailureException(Failure())
      }
    }
     "match a zero as a None" in {
      Zero match {
        case MaybeOne(opt) => opt must_== None
        case _ => throw new FailureException(Failure())
      }
    }

    "doesn't match a many" in {
      Many(Seq(1,2,3)) match {
        case MaybeOne(opt) => throw new FailureException(Failure())
        case _ => success("passed")
      }
    }
  }

  "Concatenation" should {
    "Create a One from a Zero and One" in {
      Zero ++ One(1) must_== One(1)
      One(1) ++ Zero must_== One(1)
    }
    "Create a many from ones and manys" in {
      One(1) ++ One(2) must_== Many(Seq(1,2))
      One(1) ++ Many(Seq(2,3)) must_== Many(Seq(1,2,3))
    }
  }

  "Filter" should {
    "Cut a many down to a one if one value is left" in {
      Many(Seq(1,2,3)).filter(_ % 2 == 0) must_== One(2)
    }
    "Cut down to a zero if nothing is left" in {
      One(2).filter(_ > 3) must_== Zero
    }
  }

  "Map" should {
    "maintain structure but change type" in {
      One(1).map(x => s"num: $x") must_== One("num: 1")
      Many(Seq(2,3)).map(x => s"num: $x") must_== Many(Seq("num: 2","num: 3"))
    }
  }

  "FlatMap" should {
    "Zero maps to Zero" in {
      Zero.flatMap(_ => One(3)) must_== Zero
    }
    "One maps to new format" in {
      One(3).flatMap(x => One(s"3")) must_== One("3")
    }
    "Many maps to a many" in {
      Many(Seq(1,2,3)).flatMap(x => One(s"$x")) must_== Many(Seq("1","2","3"))
    }
    "map to a zero gets a zero" in {
      Many(Seq(1,2,3)).flatMap(x => Zero) must_== Zero
    }
    "one to many gets a many" in {
      One(2).flatMap(x => Many(Seq(x, x + 1))) must_== Many(Seq(2, 3))
    }
  }
}
