name := "FunK"

organization := "org.bitbucket.alex_richardson"

version := "0.2"

scalaVersion := "2.11.7"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.0"
libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "3.7" % "test")
