// Your profile name of the sonatype account. The default is the same with the organization value
sonatypeProfileName := "org.bitbucket.alex_richardson"

// To sync with Maven central, you need to supply the following information:
pomExtra in Global := {
  <url>https://bitbucket.org/alex_richardson/funk</url>
  <licenses>
    <license>
      <name>Apache 2</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:https://bitbucket.org/alex_richardson/funk.git</connection>
    <developerConnection>scm:git:https://alex_richardson@bitbucket.org/alex_richardson/funk.git</developerConnection>
    <url>https://bitbucket.org/alex_richardson/funk</url>
  </scm>
  <developers>
    <developer>
      <id>alex_richardson</id>
      <name>Alex Richardson</name>
    </developer>
  </developers>
}
