![Alt text](http://i.imgur.com/uzomiog.png?1)

FunK - Functional Kit

* Free Monad/Interpreter abstraction
* Dumping ground for cool functional stuff

Add to built.sbt: 
``` libraryDependencies += "org.bitbucket.alex_richardson" %% "funk" % "0.2" ```

# Interpreter how to #

Extend the `Service[T]` trait with your domain specific operation, eg. `object GetCurrentTime extends Service[Long]` 

Make the call using a fetch

``` val time: Interpretable[Long] = Request.fetch(GetCurrentTime) ```

Create a service interpreter to run this, lets assume getting the current time is an async request so we're getting a future back

```
val timeIntepreter = new ServiceInterpreter[Future] {
    override def apply[A](service: Service[A]): Future[A] = service match {
      case GetCurrentTime => Future(System.currentTimeMillis())
    }
  }
```

Now run it all together with a request interpreter (and an implicit execution context)

```
val interpreter = RequestInterpreter(timeIntepreter)(FutureTransformer())

val result: Future[Long] = time.foldMap(interpreter)(FutureTransformer())
```
(Tip: The `FutureTransformer()` could be provided implicitly, scalaz monads can be grabbed with `import scalaz._`, you can also skip returning a future altogether by using the scalaz `Id` monad as the type of your service interpreter)

Awaiting the result will get you the value

```
println(Await.result(result, 5 seconds))

1453661382792
```


TODO:
 - Generify the 'Service' trait further, operate  on some subtype of service, carry that type on each request type as a phantom type. That way can run with interpreters that only know of a subdomain of Service'